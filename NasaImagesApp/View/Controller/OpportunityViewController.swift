//
//  OpportunityViewController.swift
//  NasaImagesApp
//
//  Created by Ali Şengür on 13.11.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit

class OpportunityViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    
    
    
    //MARK: - Properties
    let apiService = APIManager()
    var viewModel: PhotosViewModel!
    let activityView = UIActivityIndicatorView(style: .large)
    var indexOfPageToRequest = 1
    var loadingStatus = false
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = PhotosViewModel(apiService: apiService)
        viewModel.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        searchBar.delegate = self
        setupUI()
        initVM()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setCollectionViewItemSize()
    }
    
    
    // setup UI
    fileprivate func setupUI() {
        // set right bar button(search button)
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchButtonAction))
        self.navigationItem.rightBarButtonItem = rightBarButton
        searchBar.showsCancelButton = true
        
        self.view.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = self.view.center
        activityView.startAnimating() // start activity indicator
        
    }

    
    // initialize spinner and viewModel
    fileprivate func initVM() {
        viewModel.getPhotos(rover: "opportunity", page: indexOfPageToRequest)
        viewModel.loadingFinished = { // stop activity indicator
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                    self.collectionView?.alpha = 1
                    self.activityView.stopAnimating()
                }, completion: nil)
            }
        }
    }
    
    //MARK: - setup collection view
    fileprivate func setCollectionViewItemSize(){
        let numberOfItemsPerRow: CGFloat = 3
        let lineSpacing: CGFloat = 5
        let spacingBetweenItems: CGFloat = 5
        let width = (collectionView.frame.width - (numberOfItemsPerRow - 1) * spacingBetweenItems) / numberOfItemsPerRow
        let height = width
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout ///  UICollectionViewFlowLayout have an itemSize
        layout.itemSize = CGSize(width: width, height: height)
        layout.sectionInset = UIEdgeInsets.zero
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = lineSpacing
        layout.minimumInteritemSpacing = spacingBetweenItems
        
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    
    @objc private func searchButtonAction() {
        self.navigationItem.setRightBarButton(nil, animated: true)
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
        self.collectionView.isHidden = true
    }
}




//MARK: - Protocol Delegate pattern for MVVM
extension OpportunityViewController: ViewControllerDelegate {
    func reloadCollectionView() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}



extension OpportunityViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UIPopoverPresentationControllerDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OpportunityCell", for: indexPath) as! OpportunityCell
        let photo = viewModel.photos[indexPath.row]
        cell.configure(with: photo)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let popUpVC = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
        popUpVC.modalPresentationStyle = .overCurrentContext
        popUpVC.modalTransitionStyle = .crossDissolve
        popUpVC.preferredContentSize = CGSize(width: 300, height: 300)
        let pVC = popUpVC.popoverPresentationController
        pVC?.permittedArrowDirections = .any
        pVC?.delegate = self
        pVC?.sourceView = self.collectionView
        popUpVC.configure(with: viewModel!.photos[indexPath.row])
        present(popUpVC, animated: true, completion: nil)
    }
    
    
    
    // Infinite Scrolling
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if (offsetY > contentHeight - scrollView.frame.size.height) && !loadingStatus {
            indexOfPageToRequest += 1
            loadData()
        }
    }
    
    func loadData() {
        if !loadingStatus { // otherwise we are already loading data
            loadingStatus = true
            DispatchQueue.main.async {
                self.viewModel.getPhotos(rover: "opportunity", page: self.indexOfPageToRequest)
                self.viewModel.loadingFinished = { // closure callback
                    self.loadingStatus = false   // if photos is loaded, loading status is "false"
                }
            }
        }
    }

}


extension OpportunityViewController: UISearchBarDelegate {
    
    // remove search bar from navigation item when clicked cancel button
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationItem.titleView = nil
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchButtonAction))
        self.navigationItem.rightBarButtonItem = rightBarButton
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        viewModel.photos.removeAll()
        viewModel.getPhotos(rover: "opportunity", page: 1) // loads all photos again when clicked cancel button
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.view.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = self.view.center
        activityView.startAnimating() // start activity indicator
        viewModel.getPhotosFromCamera(rover: "opportunity", camera: searchText)
        viewModel.photos.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        if searchText.count > 0 {
            self.collectionView.isHidden = false
        }
        viewModel!.loadingFinished = { // callback closure
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                    self.activityView.removeFromSuperview()
                    self.activityView.stopAnimating()
                }, completion: nil)
            }
        }
    }
    
}
