//
//  PopUpViewController.swift
//  NasaImagesApp
//
//  Created by Ali Şengür on 15.11.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Kingfisher

class PopUpViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var earthDate: UILabel!
    @IBOutlet weak var cameraLabel: UILabel!
    @IBOutlet weak var roverNameLabel: UILabel!
    @IBOutlet weak var roverStatusLabel: UILabel!
    @IBOutlet weak var launchDateLabel: UILabel!
    @IBOutlet weak var landingDateLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    public func configure(with photo: Photos) {
        let path = photo.imgSrc!
        let url = URL(string: path)
        DispatchQueue.main.async {
            self.imageView.kf.setImage(with: url)
            self.earthDate.text = photo.earthDate
            self.cameraLabel.text = photo.camera?.name
            self.roverNameLabel.text = photo.rover?.name
            self.roverStatusLabel.text = photo.rover?.status
            self.launchDateLabel.text = photo.rover?.launchDate
            self.landingDateLabel.text = photo.rover?.landingDate
        }
    }

    @IBAction func removePopUp(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
