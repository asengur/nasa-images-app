//
//  CuriosityViewModel.swift
//  NasaImagesApp
//
//  Created by Ali Şengür on 13.11.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation



protocol ViewModelDelegate {
    func getPhotos(rover: String, page: Int)
    func getPhotosFromCamera(rover: String, camera: String)
}



protocol ViewControllerDelegate {
    func reloadCollectionView()
}



class PhotosViewModel: ViewModelDelegate {
    
    // properties
    var photos: [Photos] = [Photos]()
    var delegate: ViewControllerDelegate?
    var loadingFinished: (() -> Void)?
    
    
    let apiService: APIManagerProtocol
    
    init(apiService: APIManagerProtocol = APIManager()) { // dependency injection
        self.apiService = apiService
    }
    
    
    
    
    func getPhotos(rover: String, page: Int) {
        apiService.getPhotos(rover: rover, page: page) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let photos):
                self?.photos.append(contentsOf: photos)
                self?.delegate?.reloadCollectionView()
                self?.loadingFinished!()
            }
        }
    }
    
    func getPhotosFromCamera(rover: String, camera: String) {
        apiService.getPhotosFromCamera(rover: rover, camera: camera, page: 1) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let photos):
                self?.photos.removeAll()
                self?.photos.append(contentsOf: photos)
                self?.delegate?.reloadCollectionView()
                if photos.count > 0 {
                    self?.loadingFinished!()
                }
            }
        }
    }
    
}
