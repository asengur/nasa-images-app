//
//  NasaImagesAppTests.swift
//  NasaImagesAppTests
//
//  Created by Ali Şengür on 13.11.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import XCTest
@testable import NasaImagesApp

class NasaImagesAPIServiceTests: XCTestCase {

    var mockApiService: MockAPIManager!
    var sut: PhotosViewModel!
    
    override func setUp() {
        super.setUp()
        mockApiService = MockAPIManager()
        sut = PhotosViewModel(apiService: mockApiService)  // inject the mock api service to viewModel
    }

    override func tearDown() {
        sut = nil
        mockApiService = nil
        super.tearDown()
    }

    func testGetPhotos() { // behaviour test of viewModel
        
        //when
        sut.getPhotos(rover: "curiosity", page: 1)
        
        //assert
        XCTAssert(mockApiService!.isGetPhotosCalled)
    }
    
    
    func testGetPhotosFromCamera() {
        
        sut.getPhotosFromCamera(rover: "curiosity", camera: "FHAZ")
        
        XCTAssert(mockApiService!.isGetPhotosFromCameraCalled)
    }
    

}
