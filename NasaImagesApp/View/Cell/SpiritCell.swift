//
//  SpiritCell.swift
//  NasaImagesApp
//
//  Created by Ali Şengür on 16.11.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit

class SpiritCell: UICollectionViewCell {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    //MARK: - Configure cell
    public func configure(with photo: Photos) {
        let path = photo.imgSrc!
        let url = URL(string: path)
        
        DispatchQueue.main.async {
            self.backgroundImageView.kf.setImage(with: url)
        }
    }
}
