//
//  APIManager.swift
//  NasaImagesApp
//
//  Created by Ali Şengür on 13.11.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import Alamofire


protocol APIManagerProtocol { // dependency injection
    func getPhotos(rover: String, page: Int, completion: @escaping(Result<[Photos], AFError>) -> Void)
    func getPhotosFromCamera(rover: String, camera: String, page: Int, completion: @escaping(Result<[Photos], AFError>) -> Void)
}



//MARK: - API Manager
final class APIManager: APIManagerProtocol {
    
    // properties
    static let shared = APIManager()
    let apiKey = "UvNDxA2VYfLewu2McVBFidS4tESsrT4crTp1ml8H"
    
    func getPhotos(rover: String, page: Int, completion: @escaping(Result<[Photos], AFError>) -> Void) {
        // Get request with alamofire
        AF.request("https://api.nasa.gov/mars-photos/api/v1/rovers/\(rover)/photos?sol=1000&api_key=\(apiKey)&page=\(page)")
            .validate()
            .responseDecodable(of: Results.self){ response in
                guard let photos = response.value else {
                    completion(.failure(response.error!))
                    return
                }
                completion(.success(photos.photos!))
        }
    }
    
    
    func getPhotosFromCamera(rover: String, camera: String, page: Int, completion: @escaping(Result<[Photos], AFError>) -> Void) {
        AF.request("https://api.nasa.gov/mars-photos/api/v1/rovers/\(rover)/photos?sol=1000&camera=\(camera)&api_key=\(apiKey)&page=\(page)")
            .validate()
            .responseDecodable(of: Results.self){ response in
                guard let photos = response.value else {
                    completion(.failure(response.error!))
                    return
                }
                completion(.success(photos.photos!))
        }
    }
}
