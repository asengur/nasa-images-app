//
//  NasaImages.swift
//  NasaImagesApp
//
//  Created by Ali Şengür on 13.11.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation


struct Results: Codable {
    let photos: [Photos]?
    
}

struct Photos: Codable {
    let id: Int?
    let camera: CameraDetails?
    let imgSrc: String?
    let earthDate: String?
    let rover: Rover?
    
    private enum CodingKeys: String, CodingKey {
        case id, camera, imgSrc = "img_src", earthDate = "earth_date", rover
    }
}


struct CameraDetails: Codable {
    let id: Int?
    let name: String?
    let roverId: Int?
    let fullName: String?
    
    private enum CodingKeys: String, CodingKey {
        case id, name, roverId = "rover_id", fullName = "full_name"
    }
}


struct Rover: Codable {
    let id: Int?
    let name: String?
    let landingDate: String?
    let launchDate: String?
    let status: String?
    
    private enum CodingKeys: String, CodingKey {
        case id, name, landingDate = "landing_date", launchDate = "launch_date", status
    }
}
