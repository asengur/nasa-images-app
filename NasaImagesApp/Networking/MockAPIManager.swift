//
//  MockAPIManager.swift
//  NasaImagesApp
//
//  Created by Ali Şengür on 17.11.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import Alamofire


class MockAPIManager: APIManagerProtocol {
    
    var isGetPhotosCalled = false
    var isGetPhotosFromCameraCalled = false
    
    
    func getPhotos(rover: String, page: Int, completion: @escaping (Result<[Photos], AFError>) -> Void) {
        isGetPhotosCalled = true
    }
    
    
    func getPhotosFromCamera(rover: String, camera: String, page: Int, completion: @escaping (Result<[Photos], AFError>) -> Void) {
        isGetPhotosFromCameraCalled = true
    }
    
    
}
