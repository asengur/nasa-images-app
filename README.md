Projeyi **MVVM** mimarisini ile geliştirdim.   
View ile ViewModel arasında data binding işlemleri için **delegate pattern** ve **closure**ları kullandım.    
API istekleri için **Alamofire**, image caching işlemleri için **Kingfisher** kullandım.    
Bu proje ile ilk defa **Unit Test** yazmaya çalıştım.         

![curiosity-1.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/curiosity-1.png)
![curiosity-2.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/curiosity-2.png)
![curiosity-3.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/curiosity-3.png)
![curiosity-4.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/curiosity-4.png)
![curiosity-5.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/curiosity-5.png)
![curiosity-6.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/curiosity-6.png)
![opportunity-1.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/opportunity-1.png)
![opportunity-2.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/opportunity-2.png)
![spirit-1.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/spirit-1.png)
![spirit-2.png](https://bitbucket.org/asengur/nasa-images-app/raw/master/NasaImagesApp/Screenshots/spirit-2.png)